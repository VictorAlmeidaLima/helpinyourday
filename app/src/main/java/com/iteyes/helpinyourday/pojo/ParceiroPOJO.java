package com.iteyes.helpinyourday.pojo;

import java.io.Serializable;

public class ParceiroPOJO implements Serializable {

    private String codParc;
    private String nomeParc;
    private String ativo;
    private String tipoPessoa;
    private int matriz;
    private String cgc_cpf;
    private String cep;
    private String numero;
    private String complemento;
    private String telefone;
    private String email;
    private Double latitude;
    private Double longitude;

    public String fildCODPARC = "CODPARC";
    public String fildNOMEPARC = "NOMEPARC";
    public String fildATIVO = "ATIVO";
    public String fildTIPOPESSOA = "TIPOPESSOA";
    public String fildMATRIZ = "MATRIZ";
    public String fildCGC_CPF = "CGC_CPF";
    public String fildCEP = "CEP";
    public String fildNUMERO = "NUMERO";
    public String fildCOMPLEMENTO = "COMPLEMENTO";
    public String fildTELEFONE = "TELEFONE";
    public String fildEMAIL = "EMAIL";
    public String fildLATITUDE = "LATITUDE";
    public String fildLONGITUDE = "LONGITUDE";

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getCodParc() {
        return codParc;
    }

    public void setCodParc(String codParc) {
        this.codParc = codParc;
    }

    public String getNomeParc() {
        return nomeParc;
    }

    public void setNomeParc(String nomeParc) {
        this.nomeParc = nomeParc;
    }

    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }

    public String getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(String tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public int getMatriz() {
        return matriz;
    }

    public void setMatriz(int matriz) {
        this.matriz = matriz;
    }

    public String getCgc_cpf() {
        return cgc_cpf;
    }

    public void setCgc_cpf(String cgc_cpf) {
        this.cgc_cpf = cgc_cpf;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @Override
    public String toString() {
        return "ParceiroPOJO{" +
                "codParc='" + codParc + '\'' +
                ", nomeParc='" + nomeParc + '\'' +
                ", ativo='" + ativo + '\'' +
                ", tipoPessoa='" + tipoPessoa + '\'' +
                ", matriz=" + matriz +
                ", cgc_cpf='" + cgc_cpf + '\'' +
                ", cep='" + cep + '\'' +
                ", numero='" + numero + '\'' +
                ", complemento='" + complemento + '\'' +
                ", telefone='" + telefone + '\'' +
                ", email='" + email + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
