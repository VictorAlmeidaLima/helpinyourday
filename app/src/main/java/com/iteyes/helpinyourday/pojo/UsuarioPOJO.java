package com.iteyes.helpinyourday.pojo;

public class UsuarioPOJO {

    String id;
    String chave;
    String email;
    String nome;
    Double saldoTotalHoras = 0.0;
    Double saldoTotalHorasMes = 0.0;
    String horasMesTotal = "00:00:00";
    String foto;

    public String getHorasMesTotal() {
        return horasMesTotal;
    }

    public void setHorasMesTotal(String horasMesTotal) {
        this.horasMesTotal = horasMesTotal;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChave() {
        return chave;
    }

    public void setChave(String chave) {
        this.chave = chave;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getSaldoTotalHoras() {
        return saldoTotalHoras;
    }

    public void setSaldoTotalHoras(Double saldoTotalHoras) {
        this.saldoTotalHoras = saldoTotalHoras;
    }

    public Double getSaldoTotalHorasMes() {
        return saldoTotalHorasMes;
    }

    public void setSaldoTotalHorasMes(Double saldoTotalHorasMes) {
        this.saldoTotalHorasMes = saldoTotalHorasMes;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
