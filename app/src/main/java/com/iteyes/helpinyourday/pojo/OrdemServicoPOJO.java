package com.iteyes.helpinyourday.pojo;

import android.annotation.TargetApi;
import android.os.Build;

import com.iteyes.helpinyourday.helper.DataUtils;
import com.iteyes.helpinyourday.helper.TimeUtils;

import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import androidx.annotation.RequiresApi;


public class OrdemServicoPOJO implements Serializable {

    String chave;
    String mesAno;
    String nuOs;
    String numeroOs;
    String descricao;
    String dataOs;
    String dhInicio;
    String dhFinal;
    String codParc;
    String parceiro;
    String hrTotal;
    String duracao;

    Time horaInicio;
    Time horaFinal;
    Time horaTotal;

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.O)
    public String getDuracao(){
        if(dhInicio != null && dhFinal != null){
            return this.duracao = TimeUtils.getDuracaoHoras(this.dhInicio, this.dhFinal);
        }else{
            return "00:00:00";
        }

    }

    public void setDuracao(String duracao) {
        this.duracao = duracao;
    }

    public void getHorasProdutividade(){
//        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
//        dateFormat.format(horaInicio);
//        dateFormat.format(horaFinal);

    //TODO: CORRIGIR CAMPO DE HORATOTAL (CALCULO DE LONG NÃO FUNCIONA, está gerando uma hora de duração diferente);
        horaInicio = Time.valueOf(this.dhInicio);
        horaFinal = Time.valueOf(this.dhFinal);
        horaTotal = Time.valueOf(this.dhFinal);

        long timeInicio = horaInicio.getTime();
        long timeFinal = horaFinal.getTime();

        long timeTotal = timeInicio - timeFinal;

        horaTotal.setTime(timeTotal);
        hrTotal = String.valueOf(horaTotal);
    }

    public String getMesAno() {
        return DataUtils.getMesAnoString(this.dataOs);
    }

    public void setMesAno(String mesAno) {
        this.mesAno = mesAno;
    }

    public String getHrTotal() {
        return hrTotal;
    }

    public void setHrTotal(String hrTotal) {
        this.hrTotal = hrTotal;
    }

    public String getChave() {
        return chave;
    }

    public void setChave(String chave) {
        this.chave = chave;
    }

    public String getNuOs() {
        return nuOs;
    }

    public void setNuOs(String nuOs) {
        this.nuOs = nuOs;
    }

    public String getNumeroOs() {
        return numeroOs;
    }

    public void setNumeroOs(String numeroOs) {
        this.numeroOs = numeroOs;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDataOs() {
        return dataOs;
    }

    public void setDataOs(String dataOs) {
        this.dataOs = dataOs;
    }

    public String getDhInicio() {
        return dhInicio;
    }

    public void setDhInicio(String dhInicio) {
        this.dhInicio = dhInicio;
    }

    public String getDhFinal() {
        return dhFinal;
    }

    public void setDhFinal(String dhFinal) {
        this.dhFinal = dhFinal;
    }

    public String getCodParc() {
        return codParc;
    }

    public void setCodParc(String codParc) {
        this.codParc = codParc;
    }

    public String getParceiro() {
        return parceiro;
    }

    public void setParceiro(String parceiro) {
        this.parceiro = parceiro;
    }
}
