package com.iteyes.helpinyourday.model;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.iteyes.helpinyourday.pojo.UsuarioPOJO;

public class UsuarioModel {

    UsuarioPOJO usuarioPOJO = new UsuarioPOJO();

    public UsuarioPOJO getUsuarioPOJO() {
        return usuarioPOJO;
    }

    public void setUsuarioPOJO(UsuarioPOJO usuarioPOJO) {
        this.usuarioPOJO = usuarioPOJO;
    }

    public UsuarioModel(){
        getUsuarioLogadoFirebase();
    }

    public void getUsuarioLogadoFirebase(){
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            usuarioPOJO.setNome(currentUser.getDisplayName());
            usuarioPOJO.setFoto(currentUser.getPhotoUrl().getPath());
            usuarioPOJO.setEmail(currentUser.getEmail());

            //TODO:COLOCAR RESTANTE DOS ATRIBUTOS BUSCANDO DO FIREBASE
        }
    }
}
