package com.iteyes.helpinyourday.snk;


import okhttp3.*;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class SWHelper {
    OkHttpClient client;
    MediaType mediaType;
    HashMap<String,String> headers;
    String url;
    String nameService;
    String token;

    public SWHelper() throws Exception {
        openSession();
    }

    public void openSession() throws Exception {
        client = new OkHttpClient();
        token = getJSession();
        mediaType = MediaType.parse("text/xml");
        url = "http://skw.sankhya.com.br/";
        headers = new HashMap<>();
        headers.put("Host", "localhost:8180");
        headers.put("Accept-Encoding", "gzip, deflate");
        headers.put("Connection", "keep-alive");
        headers.put("cache-control", "no-cache");
        headers.put("Cookie", "JSESSIONID="+token);
    }

    public Document post(String nameService, String module, HashMap<String,String> headers, String bodyRequest ) throws Exception {
        this.nameService = nameService;
        RequestBody body = RequestBody.create(mediaType, bodyRequest);
        Request.Builder request = request(module,headers).post(body);
        Response response = client.newCall(request.build()).execute();
        return buildDocument(response.body().byteStream());
    }

    public Response get(String nameService,String module, HashMap<String,String> headers) throws Exception {
        this.nameService = nameService;
        Request.Builder request = request(module,headers).get();
        return client.newCall(request.build()).execute();
    }

    private String getJSession() throws Exception {
        MediaType mediaType = MediaType.parse("text/xml");
        RequestBody body = RequestBody.create(mediaType, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n<serviceRequest serviceName=\"MobileLoginSP.login\">\n\t<requestBody>\n\t\t<NOMUSU>VICTOR.ALCIDES</NOMUSU>\n\t\t<INTERNO>99115138</INTERNO>\n\t</requestBody>\n</serviceRequest>");
        Request request = new Request.Builder()
                .url("https://skw.sankhya.com.br/mge/service.sbr?serviceName=MobileLoginSP.login")
                .post(body)
                .addHeader("Content-Type", "text/xml")
                .addHeader("User-Agent", "PostmanRuntime/7.15.2")
                .addHeader("Accept", "*/*")
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Accept-Encoding", "gzip, deflate")
                .addHeader("Connection", "keep-alive")
                .addHeader("cache-control", "no-cache")
                .build();
        Response response = client.newCall(request).execute();
        Document document = buildDocument(response.body().byteStream());
        return document.getElementsByTagName("jsessionid").item(0).getTextContent();
    }

    private Document buildDocument(InputStream inputStream) throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        return db.parse(inputStream);
    }

    private Request.Builder request(String modulo, HashMap<String,String> headers) throws Exception {
        Request.Builder request = new Request.Builder();

        if(headers != null)
            for(Map.Entry<String, String> head : headers.entrySet())
                request.addHeader(head.getKey(), head.getValue());

            request = request.url(
                new StringBuffer(url).append(modulo).append("/").append("service.sbr?serviceName=")
                        .append(nameService).append("&mgeSession=").append(token)
                        .toString()
        );
        return request;
    }




}


