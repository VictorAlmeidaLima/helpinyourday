package com.iteyes.helpinyourday.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iteyes.helpinyourday.R;
import com.iteyes.helpinyourday.pojo.OrdemServicoPOJO;
import com.iteyes.helpinyourday.pojo.ParceiroPOJO;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class OrdemServicoAdapter extends RecyclerView.Adapter<OrdemServicoAdapter.MyViewHoler> {

    private List<OrdemServicoPOJO> listaOrdemServicoPOJO;

    public OrdemServicoAdapter(List<OrdemServicoPOJO> lista) {
        this.listaOrdemServicoPOJO = lista;

        if(listaOrdemServicoPOJO == null){
            listaOrdemServicoPOJO = new ArrayList<>();
        }
    }

    @NonNull
    @Override
    public MyViewHoler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLista = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lista_ordem_servico_adapter,parent,false);

        return new MyViewHoler(itemLista);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHoler myViewHoler, int i) {
        OrdemServicoPOJO ordemServicoPOJO = listaOrdemServicoPOJO.get(i);

        myViewHoler.dtOs.setText(ordemServicoPOJO.getDataOs());
        myViewHoler.parc.setText(ordemServicoPOJO.getParceiro());
    }

    @Override
    public int getItemCount() {
        return listaOrdemServicoPOJO.size();
    }

    public class MyViewHoler extends RecyclerView.ViewHolder {

        TextView dtOs;
        TextView dhInicio;
        TextView dhFinal;
        TextView parc;

        public MyViewHoler(@NonNull View itemView) {

            super(itemView);
            dtOs = itemView.findViewById(R.id.txt_dtOS);
            parc = itemView.findViewById(R.id.txt_parceiro);
        }
    }

    public void filterList(List<OrdemServicoPOJO> filterdNames) {
        this.listaOrdemServicoPOJO = filterdNames;
        notifyDataSetChanged();
    }
}
