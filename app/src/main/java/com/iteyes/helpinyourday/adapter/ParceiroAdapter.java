package com.iteyes.helpinyourday.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iteyes.helpinyourday.R;
import com.iteyes.helpinyourday.pojo.ParceiroPOJO;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ParceiroAdapter extends RecyclerView.Adapter<ParceiroAdapter.MyViewHoler> {

    private List<ParceiroPOJO> listaParceiroPOJO;

    public ParceiroAdapter(List<ParceiroPOJO> lista) {
        this.listaParceiroPOJO = lista;

        if(listaParceiroPOJO == null){
            listaParceiroPOJO = new ArrayList<>();
        }
    }


    @NonNull
    @Override
    public MyViewHoler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLista = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lista_parceiro_adapter,parent,false);

        return new MyViewHoler(itemLista);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHoler myViewHoler, int i) {
        ParceiroPOJO parceiroPOJO = listaParceiroPOJO.get(i);

        myViewHoler.parceiro.setText(parceiroPOJO.getNomeParc());
        myViewHoler.email.setText(parceiroPOJO.getEmail());
    }

    @Override
    public int getItemCount() {
        return listaParceiroPOJO.size();
    }

    public class MyViewHoler extends RecyclerView.ViewHolder {

        TextView parceiro;
        TextView email;

        public MyViewHoler(@NonNull View itemView) {

            super(itemView);
            email = itemView.findViewById(R.id.txt_email);
            parceiro = itemView.findViewById(R.id.textParceiro);
        }
    }

    public void filterList(List<ParceiroPOJO> filterdNames) {
        this.listaParceiroPOJO = filterdNames;
        notifyDataSetChanged();
    }
}
