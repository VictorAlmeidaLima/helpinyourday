package com.iteyes.helpinyourday.database;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.core.DatabaseConfig;
import com.google.firebase.database.core.RepoInfo;
import com.iteyes.helpinyourday.Singleton;

import androidx.appcompat.app.AppCompatActivity;

public class FirebaseDatabaseAcess extends AppCompatActivity {

        private static FirebaseDatabaseAcess instance;
        private DatabaseReference referencia = FirebaseDatabase.getInstance().getReference();

    public static synchronized FirebaseDatabaseAcess getFirebaseInstance(){
        if(instance == null){
            instance = new FirebaseDatabaseAcess();
        }
        return instance;
    }

    public DatabaseReference getReferencia() {
        return referencia;
    }

    public void setReferencia(DatabaseReference referencia) {
        this.referencia = referencia;
    }
}
