package com.iteyes.helpinyourday.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.iteyes.helpinyourday.dao.ParceiroDAO;
import com.iteyes.helpinyourday.old.DbHelper;
import com.iteyes.helpinyourday.pojo.ParceiroPOJO;

import java.util.ArrayList;
import java.util.List;

public class DatabaseAccess{

    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;
    private ParceiroDAO parceiroDAO;

    private SQLiteDatabase read;
    private SQLiteDatabase write;

    /**
     * Construtor privado
     * @param context
     */
    public DatabaseAccess(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }

    /**
     * Retorna um singleton de DatabaseAccess
     *
     * @param context
     */
    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    /**
     * Abre a conexão
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Fecha a conexão
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

//    public List<ParceiroPOJO> listarParceiros(){
//       return this.listarParceiros();
//    }

    public List<ParceiroPOJO> listarParceiros() {

        List<ParceiroPOJO> parceiroPOJOS = new ArrayList<>();

        String SQL = " SELECT CODPARC,NOMEPARC,ATIVO,TIPOPESSOA,MATRIZ,CGC_CPF,CEP,NUMERO,COMPLEMENTO,TELEFONE,EMAIL,LATITUDE,LONGITUDE FROM "+ DbHelper.TABELA_PARCEIRO+ " WHERE CODPARC IN (26763,26747,26717,26701,26688,26686); ";

        Cursor c = this.database.rawQuery(SQL,null);

        while(c.moveToNext()){
            ParceiroPOJO parceiroPOJO = new ParceiroPOJO();

//            parceiroPOJO.setCodParc(c.getInt(c.getColumnIndex(parceiroPOJO.CODPARC)));
//            parceiroPOJO.setNomeParc(c.getString(c.getColumnIndex(parceiroPOJO.NOMEPARC)));
//            parceiroPOJO.setAtivo(c.getString(c.getColumnIndex(parceiroPOJO.ATIVO)));
//            parceiroPOJO.setTipoPessoa(c.getString(c.getColumnIndex(parceiroPOJO.TIPOPESSOA)));
//            parceiroPOJO.setMatriz(c.getInt(c.getColumnIndex(parceiroPOJO.MATRIZ)));
//            parceiroPOJO.setCgc_cpf(c.getString(c.getColumnIndex(parceiroPOJO.CGC_CPF)));
//            parceiroPOJO.setNumero(c.getString(c.getColumnIndex(parceiroPOJO.NUMERO)));
//            parceiroPOJO.setComplemento(c.getString(c.getColumnIndex(parceiroPOJO.COMPLEMENTO)));
//            parceiroPOJO.setTelefone(c.getString(c.getColumnIndex(parceiroPOJO.TELEFONE)));
//            parceiroPOJO.setEmail(c.getString(c.getColumnIndex(parceiroPOJO.EMAIL)));
//            parceiroPOJO.setLatitude(BigDecimal.valueOf(c.getColumnIndex(parceiroPOJO.LATITUDE)));
//            parceiroPOJO.setLongitude(BigDecimal.valueOf(c.getColumnIndex(parceiroPOJO.LONGITUDE)));

            parceiroPOJOS.add(parceiroPOJO);
        }

        return parceiroPOJOS;
    }


}
