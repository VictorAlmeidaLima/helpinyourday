package com.iteyes.helpinyourday.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.google.firebase.database.DatabaseReference;
import com.iteyes.helpinyourday.R;
import com.iteyes.helpinyourday.Singleton;
import com.iteyes.helpinyourday.adapter.OrdemServicoAdapter;
import com.iteyes.helpinyourday.adapter.ParceiroAdapter;
import com.iteyes.helpinyourday.dao.OrdemServicoDAO;
import com.iteyes.helpinyourday.dao.ParceiroDAO;
import com.iteyes.helpinyourday.helper.RecyclerItemClickListener;
import com.iteyes.helpinyourday.pojo.OrdemServicoPOJO;
import com.iteyes.helpinyourday.pojo.ParceiroPOJO;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ParceirosActivity extends AppCompatActivity {

    private List<ParceiroPOJO> listaParceiros = new ArrayList<>();
    private List<ParceiroPOJO> listaParceirosFiltrados = new ArrayList<>();
    private boolean filtrandoParceiro = false;
    private ParceiroAdapter parceiroAdapter;
    private RecyclerView recyclerView;
    private EditText edtBuscarParceiro;
    private ParceiroPOJO parceiroSelecionado;
    ParceiroAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parceiros);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView = findViewById(R.id.recyclerViewParceiros);
        edtBuscarParceiro = findViewById(R.id.edtBuscarParceiro);

        carregarListaParceiros();
        carregarFirebase();

        edtBuscarParceiro.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
                filtrandoParceiro = false;
            }
        });


        //Evento de Click
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(
                        getApplicationContext(),
                        recyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            }

                            @Override
                            public void onItemClick(View view, int position) {

                                parceiroSelecionado = listaParceiros.get(position);

                                Singleton.getSingleton().setActiveParceiro(parceiroSelecionado);

                                //Envia Tarefa para tela Selecionada
                                Intent intent = new Intent(ParceirosActivity.this,DetalheParceiroActivity.class);
                                intent.putExtra("Parceiro Selecionado",parceiroSelecionado);

                                startActivity(intent);

                            }

                            @Override
                            public void onLongItemClick(View view, int position) {
                                parceiroSelecionado = listaParceiros.get(position);

                                AlertDialog.Builder dialog = new AlertDialog.Builder(ParceirosActivity.this);

                                dialog.setTitle("Confirmar Exclusão");
                                dialog.setMessage("Excluir a OS "+parceiroSelecionado.getCodParc()+" ?");

                                dialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        ParceiroDAO parceiroDAO = new ParceiroDAO(getApplicationContext());
                                        if(parceiroDAO.deletar(parceiroSelecionado)){
                                            carregarListaParceiros();
                                            carregarFirebase();
                                            Toast.makeText(ParceirosActivity.this, "Parceiro deletado", Toast.LENGTH_SHORT).show();
                                        }else{
                                            Toast.makeText(ParceirosActivity.this, "Erro ao deletar Parceiro", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });

                                dialog.setNegativeButton("Não",null);

                                dialog.create();
                                dialog.show();
                            }
                        }
                )
        );

    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<ParceiroPOJO> filterdNames = new ArrayList<ParceiroPOJO>();
        String nomeParc;
        for (ParceiroPOJO parceiro : Singleton.getSingleton().getParceirosList()) {
            nomeParc = parceiro.getNomeParc();
            if (nomeParc.toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(parceiro);
            }
        }
        //calling a method of the adapter class and passing the filtered list
        adapter.filterList(filterdNames);
        listaParceiros = filterdNames;
    }

    private void openParceiroActivity(int index){
        Singleton.getSingleton().setActiveParceiro(index);
        Intent intent = new Intent(this, DetalheParceiroActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        carregarListaParceiros();
//        carregarFirebase();
        super.onStart();
    }

    public void carregarListaParceiros(){
        ParceiroDAO parceiroDAO = new ParceiroDAO(getApplicationContext());
        listaParceiros = parceiroDAO.listarParceiros();

        adapter = new ParceiroAdapter(listaParceiros);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    public void carregarFirebase(){
        ParceiroDAO parceiroDAO = new ParceiroDAO(getApplicationContext());
        for (ParceiroPOJO pojo : Singleton.getSingleton().getParceirosList()) {
            parceiroDAO.salvarParceiro(pojo);
        };

    }

}
