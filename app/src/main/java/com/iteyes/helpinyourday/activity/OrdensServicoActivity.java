package com.iteyes.helpinyourday.activity;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.iteyes.helpinyourday.R;
import com.iteyes.helpinyourday.Singleton;
import com.iteyes.helpinyourday.adapter.OrdemServicoAdapter;
import com.iteyes.helpinyourday.dao.OrdemServicoDAO;
import com.iteyes.helpinyourday.database.FirebaseDatabaseAcess;
import com.iteyes.helpinyourday.helper.Base64Custom;
import com.iteyes.helpinyourday.helper.DataUtils;
import com.iteyes.helpinyourday.helper.RecyclerItemClickListener;
import com.iteyes.helpinyourday.model.UsuarioModel;
import com.iteyes.helpinyourday.pojo.OrdemServicoPOJO;
import com.iteyes.helpinyourday.pojo.ParceiroPOJO;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class OrdensServicoActivity extends AppCompatActivity {

    private List<OrdemServicoPOJO> listaOrdemServico = new ArrayList<>();
    private List<OrdemServicoPOJO> listaOrdemServicoFirebase = new ArrayList<>();
    private RecyclerView recyclerView;
    private OrdemServicoPOJO ordemServicoSelecionada;
    private OrdemServicoAdapter adapter;
    private EditText edtBuscarOS;
    private UsuarioModel usuarioModel = new UsuarioModel();

    DatabaseReference ordemServicoFirebase = FirebaseDatabaseAcess.getFirebaseInstance().getReferencia();

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ordens_servico);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView = findViewById(R.id.recyclerViewOrdenServico);
        edtBuscarOS = findViewById(R.id.edtBuscarOS);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Singleton.getSingleton().setActiveOrdemServico(new OrdemServicoPOJO());
                openOrdenServicosActivity();
            }
        });

        ListarOrdensServicoFirebase();

        edtBuscarOS.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });


        //Evento de Click
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(
                        getApplicationContext(),
                        recyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            }

                            @Override
                            public void onItemClick(View view, int position) {

                                //Recuperar Tarefa para Edição
                                ordemServicoSelecionada = listaOrdemServico.get(position);

                                Singleton.getSingleton().setActiveOrdemServico(ordemServicoSelecionada);

                                //Envia Tarefa para tela Selecionada
                                Intent intent = new Intent(OrdensServicoActivity.this,DetalheOrdemServicoActivity.class);
                                intent.putExtra("Ordem Servico Selecionada",ordemServicoSelecionada);

                                startActivity(intent);

                            }

                            @Override
                            public void onLongItemClick(View view, int position) {
                                ordemServicoSelecionada = listaOrdemServico.get(position);

                                AlertDialog.Builder dialog = new AlertDialog.Builder(OrdensServicoActivity.this);

                                dialog.setTitle("Confirmar Exclusão");
                                dialog.setMessage("Excluir a OS "+ordemServicoSelecionada.getNuOs()+" ?");

                                dialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        OrdemServicoDAO ordemServicoDAO = new OrdemServicoDAO(getApplicationContext());
                                        if(ordemServicoDAO.deletarOrdemServico(ordemServicoSelecionada)){
                                            carregarListaOrdemServico();
                                            Toast.makeText(OrdensServicoActivity.this, "OS Deletada", Toast.LENGTH_SHORT).show();
                                        }else{
                                            Toast.makeText(OrdensServicoActivity.this, "Erro ao Deletar OS", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });

                                dialog.setNegativeButton("Não",null);

                                dialog.create();
                                dialog.show();
                            }
                        }
                )
        );

        carregarListaOrdemServico();
    }

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void ListarOrdensServicoFirebase() {

        String emailBase64 = Base64Custom.codificarStrintToBase64(usuarioModel.getUsuarioPOJO().getEmail());
        String mesAnoAtual = DataUtils.getMesAnoAtual();
        final DatabaseReference ordemServico = ordemServicoFirebase.child("tcsose").child(emailBase64).child(mesAnoAtual);

        ordemServico.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<OrdemServicoPOJO> listaOrdemServicoFirebase = new ArrayList<>();

                if(dataSnapshot.exists()) {

                    for (DataSnapshot child : dataSnapshot.getChildren()) {

                        OrdemServicoPOJO ordemServicoPOJO = child.getValue(OrdemServicoPOJO.class);
                        ordemServicoPOJO.setChave(child.getKey());
                        listaOrdemServicoFirebase.add(ordemServicoPOJO);
                    }
                    listaOrdemServico = listaOrdemServicoFirebase;
                    OrdemServicoAdapter adapter = new OrdemServicoAdapter(listaOrdemServicoFirebase);
                    recyclerView.setAdapter(adapter);
                }
//                carregarListaOrdemServico();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onStart() {
        carregarListaOrdemServico();
        super.onStart();
    }

    private void openOrdemSerivoActivity(int index){
        Singleton.getSingleton().setActiveParceiro(index);
        Intent intent = new Intent(this, DetalheOrdemServicoActivity.class);
        startActivity(intent);
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<OrdemServicoPOJO> filterdNames = new ArrayList<OrdemServicoPOJO>();
        String nomeParc;
        for (OrdemServicoPOJO ordemServico : listaOrdemServico) {
            nomeParc = ordemServico.getParceiro();
            if (nomeParc.toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(ordemServico);
            }
        }
        //calling a method of the adapter class and passing the filtered list
        adapter.filterList(filterdNames);
        listaOrdemServico = filterdNames;
    }

    public void carregarListaOrdemServico(){

//            OrdemServicoDAO ordemServicoDAO = new OrdemServicoDAO(getApplicationContext());
//            listaOrdemServico = ordemServicoDAO.listaOrdemServico();

        adapter = new OrdemServicoAdapter(listaOrdemServico);
//        adapter = new RecyclerViewAdapter(OrdemServicoPOJO.this, listaOrdemServico);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    private void openOrdenServicosActivity() {
        Intent intent = new Intent(this, DetalheOrdemServicoActivity.class);
        startActivity(intent);
    }


}
