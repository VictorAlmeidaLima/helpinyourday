package com.iteyes.helpinyourday.activity;


import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.iteyes.helpinyourday.R;
import com.iteyes.helpinyourday.Singleton;
import com.iteyes.helpinyourday.dao.OrdemServicoDAO;
import com.iteyes.helpinyourday.dao.ParceiroDAO;
import com.iteyes.helpinyourday.pojo.OrdemServicoPOJO;
import com.iteyes.helpinyourday.pojo.ParceiroPOJO;

import androidx.appcompat.app.AppCompatActivity;

public class DetalheParceiroActivity extends AppCompatActivity {
    private ParceiroPOJO parceiroPOJO;
    private Button btnSalvarParceiro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_parceiro);
        parceiroPOJO = Singleton.getSingleton().getActiveParceiro();

//        EditText edtCodParc = findViewById(R.id.edt_codparc);
        btnSalvarParceiro = findViewById(R.id.btn_salvar_parc);
        EditText edtCodParc = findViewById(R.id.edt_codparc);
        TextView tvName = (TextView) findViewById(R.id.lb_nomeParceiro);
        EditText edtTelefone = findViewById(R.id.edt_telefone);
        EditText edtCnpj = findViewById(R.id.edt_cnpj);
        EditText edtEmail = findViewById(R.id.edt_email);
        EditText edtComplemento = findViewById(R.id.edt_complemento);
        EditText edtNumero = findViewById(R.id.edt_numero);
        final EditText edtCep = findViewById(R.id.edt_cep);

        edtCodParc.setText(parceiroPOJO.getCodParc());
        tvName.setText(parceiroPOJO.getNomeParc());
        edtTelefone.setText(parceiroPOJO.getTelefone());
        edtCnpj.setText(parceiroPOJO.getCgc_cpf());
        edtEmail.setText(parceiroPOJO.getEmail());
        edtComplemento.setText(parceiroPOJO.getComplemento());
        edtNumero.setText(parceiroPOJO.getNumero());
        edtCep.setText(parceiroPOJO.getCep());


        btnSalvarParceiro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParceiroDAO parceiroDAO = new ParceiroDAO(getApplicationContext());
                parceiroDAO.salvarParceiro(parceiroPOJO);
                finish();
            }
        });


        edtCep.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                parceiroPOJO.setCep(edtCep.getText().toString());
            }
        });

    }
}
