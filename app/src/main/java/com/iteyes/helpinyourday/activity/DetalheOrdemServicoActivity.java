package com.iteyes.helpinyourday.activity;

import android.os.Build;
import android.os.Bundle;
import android.util.TimeUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.iteyes.helpinyourday.R;
import com.iteyes.helpinyourday.Singleton;
import com.iteyes.helpinyourday.dao.OrdemServicoDAO;
import com.iteyes.helpinyourday.helper.DataUtils;
import com.iteyes.helpinyourday.pojo.OrdemServicoPOJO;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

public class DetalheOrdemServicoActivity extends AppCompatActivity {

    private OrdemServicoPOJO ordemServicoPOJO;
    private Button btnSalvar;
    private Button btnIniciarOS;
    private Button btnFinalizarOS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_ordemservico);
        ordemServicoPOJO = Singleton.getSingleton().getActiveOrdemServico();
        if(ordemServicoPOJO == null){
            ordemServicoPOJO = new OrdemServicoPOJO();
        }

         final EditText edtNuOS = findViewById(R.id.edt_nuOs);
         final EditText edtNumeroOS = findViewById(R.id.edt_numeroOS);
         final EditText edtDescricao = findViewById(R.id.edt_descricao);
         final EditText edtDhfinal = findViewById(R.id.edt_dhfinal);
         final EditText edtDhInicio = findViewById(R.id.edt_dhinicio);
         final EditText edtDtOs = findViewById(R.id.edt_dtOs);
         final EditText edtParceiro = findViewById(R.id.edt_parc);
         final EditText edtTotalHoras = findViewById(R.id.edt_total_horas);

        EditText edtCodParc = findViewById(R.id.edt_codparc);
        btnSalvar = findViewById(R.id.btn_salvar_os);
        btnIniciarOS = findViewById(R.id.btn_IniciarOS);
        btnFinalizarOS = findViewById(R.id.btn_FinalizarOS);

        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                OrdemServicoDAO ordemServicoDAO = new OrdemServicoDAO(getApplicationContext());

                String descricao = edtDescricao.getText().toString();
                String numeroOS = edtNumeroOS.getText().toString();
                String dhInicio = edtDhInicio.getText().toString();
                String dhFinal = edtDhfinal.getText().toString();
                String dtOS = edtDtOs.getText().toString();
                String parceiro = edtParceiro.getText().toString();
                String key = edtNuOS.getText().toString();
                String totalHoras = edtTotalHoras.getText().toString();
                String mesAno = DataUtils.getMesAnoString(dtOS);

                ordemServicoPOJO.setNumeroOs(numeroOS);
                ordemServicoPOJO.setDescricao(descricao);
                ordemServicoPOJO.setDhInicio(dhInicio);
                ordemServicoPOJO.setDhFinal(dhFinal);
                ordemServicoPOJO.setDataOs(dtOS);
                ordemServicoPOJO.setParceiro(parceiro);
                ordemServicoPOJO.setChave(key);
                ordemServicoPOJO.getHorasProdutividade();
                ordemServicoPOJO.setMesAno(mesAno);

//                ordemServicoDAO.salvarOrdemServico(ordemServicoPOJO);
                ordemServicoDAO.salvarOrdemServicoUsuarioMesAno(ordemServicoPOJO);


                finish();

            }
        });

        btnIniciarOS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    ordemServicoPOJO.setDhInicio(getNow());
                    ordemServicoPOJO.setDataOs(getDate());
                    edtDhInicio.setText(ordemServicoPOJO.getDhInicio());
                    edtDtOs.setText(ordemServicoPOJO.getDataOs());
            }
        });


        btnFinalizarOS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    ordemServicoPOJO.setDhFinal(getNow());
                    edtDhfinal.setText(ordemServicoPOJO.getDhFinal());

                    if(ordemServicoPOJO.getDhInicio() != null && ordemServicoPOJO.getDhFinal() != null ){
//                        ordemServicoPOJO.getHorasProdutividade();
                        edtTotalHoras.setText(ordemServicoPOJO.getDuracao());
                    }
            }
        });

         edtNuOS.setText(ordemServicoPOJO.getChave());
         edtDescricao.setText(ordemServicoPOJO.getDescricao());
         edtDhInicio.setText(ordemServicoPOJO.getDhInicio());
         edtDhfinal.setText(ordemServicoPOJO.getDhFinal());
         edtNumeroOS.setText(ordemServicoPOJO.getNumeroOs());
         edtDtOs.setText(ordemServicoPOJO.getDataOs());
         edtParceiro.setText(ordemServicoPOJO.getParceiro());
         edtTotalHoras.setText(ordemServicoPOJO.getDuracao());

    }

    public String getNow(){
        SimpleDateFormat formatter= new SimpleDateFormat("HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        return formatter.format(date);
    }

    public String getDate(){
        SimpleDateFormat formatter= new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date(System.currentTimeMillis());
        return formatter.format(date);
    }
}
