package com.iteyes.helpinyourday.activity;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.iteyes.helpinyourday.R;
import com.iteyes.helpinyourday.Singleton;
import com.iteyes.helpinyourday.adapter.OrdemServicoAdapter;
import com.iteyes.helpinyourday.dao.OrdemServicoDAO;
import com.iteyes.helpinyourday.database.FirebaseDatabaseAcess;
import com.iteyes.helpinyourday.helper.Base64Custom;
import com.iteyes.helpinyourday.helper.DataUtils;
import com.iteyes.helpinyourday.helper.RecyclerItemClickListener;
import com.iteyes.helpinyourday.helper.TimeUtils;
import com.iteyes.helpinyourday.model.UsuarioModel;
import com.iteyes.helpinyourday.pojo.OrdemServicoPOJO;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ConsultaOsActivity extends AppCompatActivity {

    private List<OrdemServicoPOJO> listaOrdemServico = new ArrayList<>();
    private List<OrdemServicoPOJO> listaOrdemServicoFirebase = new ArrayList<>();
    private RecyclerView recyclerView;
    private OrdemServicoPOJO ordemServicoSelecionada;
    private OrdemServicoAdapter adapter;
    private EditText edtBuscarOS;
    private MaterialCalendarView materialCalendarView;
    private FloatingActionButton fab;
    UsuarioModel usuarioModel = new UsuarioModel();
    private String mesAnoBusca = "";
    private ValueEventListener valueEventListenerOrdemServico;
    DatabaseReference ordemServicoRef = null;
    private TextView txtTotalHorasMes = null;


    DatabaseReference ordemServicoFirebase = FirebaseDatabaseAcess.getFirebaseInstance().getReferencia();

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setarObjetosInterfaceAndroid();
        buscarOsFirebase(mesAnoBusca);
        carregarInterfaceOs();
        carregarRecyclerView();
        carregarListaOrdemServico();
        carregarCalendarioOs();
    }

    private void setarObjetosInterfaceAndroid() {
        setContentView(R.layout.activity_consulta_os);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView = findViewById(R.id.recyclerViewOrdenServico);
        edtBuscarOS = findViewById(R.id.edtBuscarOS);
        fab = findViewById(R.id.fab);
        materialCalendarView = findViewById(R.id.calendar_consulta_os);
        txtTotalHorasMes = findViewById(R.id.txt_totalHorasMes);
    }

    private void carregarInterfaceOs() {

        txtTotalHorasMes.setText(usuarioModel.getUsuarioPOJO().getHorasMesTotal());

        //Botão
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Singleton.getSingleton().setActiveOrdemServico(new OrdemServicoPOJO());
                openOrdenServicosActivity();
            }
        });


        //Edit
        edtBuscarOS.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
    }

    private void carregarCalendarioOs() {

        CharSequence meses [] = {"Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"};
        materialCalendarView.setTitleMonths(meses);

        materialCalendarView.setOnMonthChangedListener(new OnMonthChangedListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
                String dtFormatada = DataUtils.formataData(date.getDate());
                mesAnoBusca =  DataUtils.getMesAnoString(dtFormatada);
                buscarOsFirebase(mesAnoBusca);
                carregarListaOrdemServico();
                txtTotalHorasMes.setText(usuarioModel.getUsuarioPOJO().getHorasMesTotal());
            }
        });

//        materialCalendarView.setOnDateChangedListener(new OnDateSelectedListener() {
//            @RequiresApi(api = Build.VERSION_CODES.O)
//            @Override
//            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
//                buscarOsFirebase(mesAnoBusca);
//                carregarListaOrdemServico();
//
//            }
//        });
    }

    private void carregarRecyclerView() {
        //Evento de Click
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(
                        getApplicationContext(),
                        recyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            }

                            @Override
                            public void onItemClick(View view, int position) {

                                //Recuperar Tarefa para Edição
                                ordemServicoSelecionada = listaOrdemServico.get(position);

                                Singleton.getSingleton().setActiveOrdemServico(ordemServicoSelecionada);

                                //Envia Tarefa para tela Selecionada
                                Intent intent = new Intent(ConsultaOsActivity.this, DetalheOrdemServicoActivity.class);
                                intent.putExtra("Ordem Servico Selecionada",ordemServicoSelecionada);

                                startActivity(intent);

                            }

                            @Override
                            public void onLongItemClick(View view, int position) {
                                ordemServicoSelecionada = listaOrdemServico.get(position);

                                AlertDialog.Builder dialog = new AlertDialog.Builder(ConsultaOsActivity.this);

                                dialog.setTitle("Confirmar Exclusão");
                                dialog.setMessage("Excluir a OS "+ordemServicoSelecionada.getNuOs()+" ?");

                                dialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                                    @RequiresApi(api = Build.VERSION_CODES.O)
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        OrdemServicoDAO ordemServicoDAO = new OrdemServicoDAO(getApplicationContext());
                                        if(ordemServicoDAO.deletarOrdemServico(ordemServicoSelecionada)){

                                            buscarOsFirebase(mesAnoBusca);
                                            carregarListaOrdemServico();

                                            Toast.makeText(ConsultaOsActivity.this, "OS Deletada", Toast.LENGTH_SHORT).show();
                                        }else{
                                            Toast.makeText(ConsultaOsActivity.this, "Erro ao Deletar OS", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });

                                dialog.setNegativeButton("Não",null);

                                dialog.create();
                                dialog.show();
                            }
                        }
                )
        );
    }

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void buscarOsFirebase(String mesAno) {

        if("".equals(mesAno)|| mesAno == null) mesAno =  DataUtils.getMesAnoAtual();

        String emailBase64 = Base64Custom.codificarStrintToBase64(usuarioModel.getUsuarioPOJO().getEmail());
        ordemServicoRef = ordemServicoFirebase.child("tcsose").child(emailBase64).child(mesAno);

        valueEventListenerOrdemServico = ordemServicoRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                List<OrdemServicoPOJO> listaOrdemServicoFirebase = new ArrayList<>();

//                if (dataSnapshot.exists()) {
                    listaOrdemServico.clear();
                    String horasMesTotal = "00:00:00";

                    for (DataSnapshot child : dataSnapshot.getChildren()) {

                        OrdemServicoPOJO ordemServicoPOJO = child.getValue(OrdemServicoPOJO.class);
                        ordemServicoPOJO.setChave(child.getKey());
                        listaOrdemServico.add(ordemServicoPOJO);

                        horasMesTotal = TimeUtils.somarHoras(horasMesTotal,ordemServicoPOJO.getDuracao());
                    }
                    usuarioModel.getUsuarioPOJO().setHorasMesTotal(horasMesTotal);
                    adapter.notifyDataSetChanged();
                    txtTotalHorasMes.setText(horasMesTotal);
//                }
//                carregarListaOrdemServico();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onStart() {
        buscarOsFirebase(mesAnoBusca);
        carregarListaOrdemServico();
        super.onStart();
    }

    @Override
    protected void onStop() {
        ordemServicoRef.removeEventListener(valueEventListenerOrdemServico);
        super.onStop();
    }

    private void openOrdemSerivoActivity(int index){
        Singleton.getSingleton().setActiveParceiro(index);
        Intent intent = new Intent(this, DetalheOrdemServicoActivity.class);
        startActivity(intent);
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<OrdemServicoPOJO> filterdNames = new ArrayList<OrdemServicoPOJO>();
        String nomeParc;
        for (OrdemServicoPOJO ordemServico : listaOrdemServico) {
            nomeParc = ordemServico.getParceiro();
            if (nomeParc.toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(ordemServico);
            }
        }
        //calling a method of the adapter class and passing the filtered list
        adapter.filterList(filterdNames);
        listaOrdemServico = filterdNames;
    }

    public void carregarListaOrdemServico(){

//            OrdemServicoDAO ordemServicoDAO = new OrdemServicoDAO(getApplicationContext());
//            listaOrdemServico = ordemServicoDAO.listaOrdemServico();

        adapter = new OrdemServicoAdapter(listaOrdemServico);
//        adapter = new RecyclerViewAdapter(OrdemServicoPOJO.this, listaOrdemServico);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    private void openOrdenServicosActivity() {
        Intent intent = new Intent(this, DetalheOrdemServicoActivity.class);
        startActivity(intent);
    }


}
