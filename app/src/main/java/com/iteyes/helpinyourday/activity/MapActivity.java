package com.iteyes.helpinyourday.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.iteyes.helpinyourday.R;
import com.iteyes.helpinyourday.Singleton;
import com.iteyes.helpinyourday.dao.OrdemServicoDAO;
import com.iteyes.helpinyourday.dao.ParceiroDAO;
import com.iteyes.helpinyourday.database.FirebaseDatabaseAcess;
import com.iteyes.helpinyourday.pojo.OrdemServicoPOJO;
import com.iteyes.helpinyourday.pojo.ParceiroPOJO;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.mancj.materialsearchbar.adapter.SuggestionsAdapter;
import com.skyfishjy.library.RippleBackground;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import model.City;
import model.CityJSONParser;

import static com.iteyes.helpinyourday.JsonController.readJsonListCity;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, NavigationView.OnNavigationItemSelectedListener {

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private PlacesClient placesClient;
    private List<AutocompletePrediction> predictionList;

    private FirebaseAuth mAuth;
    private Location mLastKnownLocation;
    private LocationCallback locationCallback;

    private MaterialSearchBar materialSearchBar;
    private View mapView;

    private Button btnOpenCityList;
    private Button btnFindListaCidade;
    private List<ParceiroPOJO> listaParceiros = new ArrayList<>();
    private List<OrdemServicoPOJO> listaOrdemServico = new ArrayList<>();

    private RippleBackground rippleBg;
    RequestQueue queue;
    private boolean buscaPorTexto = false;
    private LatLng coordPlaceFound;
    private final float DEFAULT_ZOOM = 10.0f;

    private List<Marker> listaMarker = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        setNavigationViewListener();

        carregarObjetosInterface();

        carregarListas();
        Singleton.getSingleton().setParceirosList(listaParceiros);
        Singleton.getSingleton().setOrdenServicoList(listaOrdemServico);

        carregarListaParceirosDao();
        carregarGoogleMaps();
        carregarSearchBar();
        carregarBotaoBuscarParceiros();

    }

    private void carregarObjetosInterface() {
        materialSearchBar = findViewById(R.id.searchBar);
        btnFindListaCidade = findViewById(R.id.btn_find_lista_cidade);
        rippleBg = findViewById(R.id.ripple_bg);
        queue = Volley.newRequestQueue(this);
    }

    private void carregarListaParceirosDao() {
        ParceiroDAO parceiroDAO = new ParceiroDAO(getApplicationContext());
        listaParceiros = parceiroDAO.listarParceiros();
    }

    private void carregarGoogleMaps() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapView = mapFragment.getView();
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(MapActivity.this);
        Places.initialize(MapActivity.this, getString(R.string.google_maps_api));
        placesClient = Places.createClient(this);
    }

    private void carregarBotaoBuscarParceiros() {


        btnFindListaCidade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LatLng currentMarkerLocation = mMap.getCameraPosition().target;
                rippleBg.startRippleAnimation();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        rippleBg.stopRippleAnimation();
                        LatLng coordDeviceLocation = null;
//                        btnFindListaCidade.setEnabled(false);
                        if (buscaPorTexto) {
                            coordDeviceLocation = coordPlaceFound;
                            buscaPorTexto = false;
                        } else {
                            if(mLastKnownLocation != null) {
                                coordDeviceLocation = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
                                Marker markerLocation = mMap.addMarker(new MarkerOptions().position(coordDeviceLocation));
                                markerLocation.setTitle("Localização Atual");
                            }else{
                                coordDeviceLocation = new LatLng(-19.343748,-34.34324);
                            }
                        }
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordDeviceLocation, DEFAULT_ZOOM));
//                        requestCitiesFromOpenWeather(coordDeviceLocation);
                        requestParceirosFromBaseData();
//                        btnOpenCityList.setEnabled(true);

//                        finish();
                    }
                }, 3000);

            }
        });
    }

    private void carregarSearchBar() {

        final AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();

        materialSearchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean enabled) {
//                btnFindListaCidade.setEnabled(false);
//                btnOpenCityList.setEnabled(false);
            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
                startSearch(text.toString(), true, null, true);
            }

            @Override
            public void onButtonClicked(int buttonCode) {
//                btnFindListaCidade.setEnabled(false);
//                btnOpenCityList.setEnabled(false);

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }

                if (buttonCode == MaterialSearchBar.BUTTON_NAVIGATION) {

                } else if (buttonCode == MaterialSearchBar.BUTTON_BACK) {
                    materialSearchBar.disableSearch();
//                    btnFindListaCidade.setEnabled(true);
                }
            }
        });

        materialSearchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                FindAutocompletePredictionsRequest predictionsRequest = FindAutocompletePredictionsRequest.builder()
                        .setTypeFilter(TypeFilter.ADDRESS)
                        .setSessionToken(token)
                        .setQuery(s.toString())
                        .build();
                placesClient.findAutocompletePredictions(predictionsRequest).addOnCompleteListener(new OnCompleteListener<FindAutocompletePredictionsResponse>() {
                    @Override
                    public void onComplete(@NonNull Task<FindAutocompletePredictionsResponse> task) {
                        if (task.isSuccessful()) {
                            FindAutocompletePredictionsResponse predictionsResponse = task.getResult();
                            if (predictionsResponse != null) {
                                predictionList = predictionsResponse.getAutocompletePredictions();
                                List<String> suggestionsList = new ArrayList<>();
                                for (int i = 0; i < predictionList.size(); i++) {
                                    AutocompletePrediction prediction = predictionList.get(i);
                                    suggestionsList.add(prediction.getFullText(null).toString());
                                }
                                materialSearchBar.updateLastSuggestions(suggestionsList);
                                if (!materialSearchBar.isSuggestionsVisible()) {
                                    materialSearchBar.showSuggestionsList();
                                }
                            }
                        } else {
                            Log.i("mytag", "prediction fetching task unsuccessful");
                        }
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        materialSearchBar.setSuggstionsClickListener(new SuggestionsAdapter.OnItemViewClickListener() {
            @Override
            public void OnItemClickListener(int position, View v) {
                if (position >= predictionList.size()) {
                    return;
                }
                AutocompletePrediction selectedPrediction = predictionList.get(position);
                String suggestion = materialSearchBar.getLastSuggestions().get(position).toString();
                materialSearchBar.setText(suggestion);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        materialSearchBar.clearSuggestions();
                    }
                }, 1000);
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                if (imm != null)
                    imm.hideSoftInputFromWindow(materialSearchBar.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
                final String placeId = selectedPrediction.getPlaceId();
                List<Place.Field> placeFields = Arrays.asList(Place.Field.LAT_LNG);

                FetchPlaceRequest fetchPlaceRequest = FetchPlaceRequest.builder(placeId, placeFields).build();
                placesClient.fetchPlace(fetchPlaceRequest).addOnSuccessListener(new OnSuccessListener<FetchPlaceResponse>() {
                    @Override
                    public void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
                        Place place = fetchPlaceResponse.getPlace();
                        Log.i("mytag", "Place found: " + place.getName());
                        LatLng latLngOfPlace = place.getLatLng();
                        if (latLngOfPlace != null) {
                            coordPlaceFound = latLngOfPlace;
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngOfPlace, DEFAULT_ZOOM));
                            Marker marker = mMap.addMarker(new MarkerOptions().position(latLngOfPlace));
                            marker.setTitle(place.getName());
                            buscaPorTexto = true;

//                            btnFindListaCidade.setEnabled(true);
//                            btnOpenCityList.setEnabled(false);
//                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cordGoogle3, DEFAULT_ZOOM));

//                            requestCitiesFromOpenWeather((marker.getPosition()));

//                            marker.showInfoWindow();
                            //TODO:COLOCAR METODO DE BUSCA DA API AO MARCAR
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            apiException.printStackTrace();
                            int statusCode = apiException.getStatusCode();
                            Log.i("mytag", "place not found: " + e.getMessage());
                            Log.i("mytag", "status code: " + statusCode);
                        }
                    }
                });
            }

            @Override
            public void OnItemDeleteListener(int position, View v) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void carregarListas(){
        //Lista Parceiros
        carregarListaParceirosDao();

        //Lista Ordem Servico
        OrdemServicoDAO ordemServicoDAO = new OrdemServicoDAO(getApplicationContext());
        listaOrdemServico = ordemServicoDAO.listaOrdemServico();

    }


    private void gerarMarcadorAPartirDaLista(List<City> cityList) {
        if (!cityList.isEmpty()) {
            for (City city : cityList) {
                LatLng cordGoogleSp = new LatLng(city.getLat(), city.getLog());
                Marker marker = mMap.addMarker(new MarkerOptions().position(cordGoogleSp));
                marker.setTitle(city.getName());
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.icon));
                listaMarker.add(marker);
            }
        }
    }

    private void gerarMarcadorAPartirDaListaParceiros(List<ParceiroPOJO> parceirosList) {

        int height = 110;
        int width = 110;
        BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.ic_snk_blc);
        Bitmap b=bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

        if (!parceirosList.isEmpty()) {
            for (ParceiroPOJO parceiroPOJO : parceirosList) {
                LatLng cordGoogleSp = new LatLng(parceiroPOJO.getLatitude(), parceiroPOJO.getLongitude());
                Marker marker = mMap.addMarker(new MarkerOptions().position(cordGoogleSp));
                marker.setTitle(parceiroPOJO.getNomeParc());
                marker.setIcon(BitmapDescriptorFactory.fromBitmap(smallMarker));
                listaMarker.add(marker);
            }
        }
    }

    public void carregarJson() throws Exception {
        List<City> cities = readJsonListCity("http://api.openweathermap.org/data/2.5/find?lat=55.5&lon=37.5&cnt=15&APPID=adb8e3486fe2c43287474e8d1db1d455");
        for (City city : cities) {
            System.out.print(city.toString());
        }
    }

    public void requestCitiesFromOpenWeather(LatLng latLng) {
        String openWeatherMapAPIKey = getString(R.string.open_weather_map_api);
        String requestURL = "http://api.openweathermap.org/data/2.5/find?lat=%.2f&lon=%.2f&cnt=15&APPID=%s&units=metric";
        requestURL = String.format(requestURL, latLng.latitude, latLng.longitude, openWeatherMapAPIKey);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            ArrayList<City> cityList = (ArrayList<City>) CityJSONParser.getCityList(response);
//                            Singleton.getSingleton().setCityList(cityList);
                            removerMarcadoresExistentes();
                            gerarMarcadorAPartirDaLista(cityList);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast toast = Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG);
                toast.show();
            }
        });
        queue.add(stringRequest);
    }

    public void requestParceirosFromBaseData() {

        Singleton.getSingleton().setParceirosList(listaParceiros);
        removerMarcadoresExistentes();
        gerarMarcadorAPartirDaListaParceiros(listaParceiros);
    }

    private void removerMarcadoresExistentes() {
        for (Marker marker : listaMarker) {
            marker.remove();
        }

    }


    private void openParceirosActivity() {
        Intent intent = new Intent(this, ParceirosActivity.class);
        startActivity(intent);
    }

    private void openOrdenServicosActivity() {
        Intent intent = new Intent(this, OrdensServicoActivity.class);
        startActivity(intent);
    }

    private void openConsultaOsActivity() {
        Intent intent = new Intent(this, ConsultaOsActivity.class);
        startActivity(intent);
    }


    public MapActivity getActivity() {
        return this;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        if (mapView != null && mapView.findViewById(Integer.parseInt("1")) != null) {
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 40, 180);
        }

        //check if gps is enabled or not and then request user to enable it
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);

        SettingsClient settingsClient = LocationServices.getSettingsClient(MapActivity.this);
        Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(builder.build());

        task.addOnSuccessListener(MapActivity.this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                getDeviceLocation();
            }
        });

        task.addOnFailureListener(MapActivity.this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    ResolvableApiException resolvable = (ResolvableApiException) e;
                    try {
                        resolvable.startResolutionForResult(MapActivity.this, 51);
                    } catch (IntentSender.SendIntentException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {

//                btnFindListaCidade.setEnabled(true);
//                btnOpenCityList.setEnabled(false);
                buscaPorTexto = false;

                if (materialSearchBar.isSuggestionsVisible())
                    materialSearchBar.clearSuggestions();
                if (materialSearchBar.isSearchEnabled())
                    materialSearchBar.disableSearch();
                return false;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 51) {
            if (resultCode == RESULT_OK) {
                getDeviceLocation();
            }
        }
    }

    @SuppressLint("MissingPermission")
    private void getDeviceLocation() {

        mFusedLocationProviderClient.getLastLocation()
                .addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            mLastKnownLocation = task.getResult();
                            if (mLastKnownLocation != null) {
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                                coordPlaceFound = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());

//                                btnFindListaCidade.setEnabled(true);
//                                btnOpenCityList.setEnabled(false);
                                buscaPorTexto = false;
                            } else {
                                final LocationRequest locationRequest = LocationRequest.create();
                                locationRequest.setInterval(10000);
                                locationRequest.setFastestInterval(5000);
                                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                                locationCallback = new LocationCallback() {
                                    @Override
                                    public void onLocationResult(LocationResult locationResult) {
                                        super.onLocationResult(locationResult);
                                        if (locationResult == null) {
                                            return;
                                        }
                                        mLastKnownLocation = locationResult.getLastLocation();
                                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                                        mFusedLocationProviderClient.removeLocationUpdates(locationCallback);
                                    }
                                };
                                mFusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);

                            }
                        } else {
                            Toast.makeText(MapActivity.this, "unable to get last location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        // Handle navigation view item clicks here.
        switch (menuItem.getItemId()) {

            case R.id.nav_parceiros: {
                openParceirosActivity();
                break;
            }

            case R.id.nav_apontamentos: {
//                openOrdenServicosActivity();
                openConsultaOsActivity();
                break;
            }
            case R.id.nav_logout: {
                logout();
                break;
            }
        }
        //close navigation drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);


        return true;
    }

    private void setNavigationViewListener() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        configUsuario(navigationView);
    }

    private void configUsuario(NavigationView navigationView){
        Picasso picasso = Picasso.get();

        View nav = navigationView.getHeaderView(0);

        ImageView imageView = nav.findViewById(R.id.imageView);
        TextView txt_user_nav = nav.findViewById(R.id.txt_user_nav);
        TextView txt_email_nav = nav.findViewById(R.id.txt_email_nav);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        String photo = String.valueOf(user.getPhotoUrl());

        txt_user_nav.setText(user.getDisplayName());
        txt_email_nav.setText(user.getEmail());
        picasso.load(photo).into(imageView);
    }

    private void logout(){
        FirebaseAuth.getInstance().signOut();

        GoogleSignInClient mGoogleSignInClient;

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        mGoogleSignInClient.signOut();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

}
