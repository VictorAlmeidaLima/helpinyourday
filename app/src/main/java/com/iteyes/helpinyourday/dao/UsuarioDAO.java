package com.iteyes.helpinyourday.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;

import com.google.firebase.database.DatabaseReference;
import com.iteyes.helpinyourday.database.FirebaseDatabaseAcess;
import com.iteyes.helpinyourday.helper.Base64Custom;
import com.iteyes.helpinyourday.old.DbHelper;
import com.iteyes.helpinyourday.pojo.UsuarioPOJO;

import androidx.annotation.RequiresApi;

public class UsuarioDAO {
    private SQLiteDatabase read;
    private SQLiteDatabase write;

    private DatabaseReference firebaseReferencia;

    public UsuarioDAO(Context context) {
        firebaseReferencia = FirebaseDatabaseAcess.getFirebaseInstance().getReferencia();
        DbHelper dbHelper = new DbHelper(context);
        write = dbHelper.getWritableDatabase();
        read = dbHelper.getReadableDatabase();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void salvarUsuario(UsuarioPOJO usuarioPOJO) {
        String emailBase64 = Base64Custom.codificarStrintToBase64(usuarioPOJO.getEmail());

        firebaseReferencia.child("tsiusu")
                .child(emailBase64)
                .setValue(usuarioPOJO);

    }

    public boolean deletarUsuario(UsuarioPOJO usuarioPOJO) {
        try {
            DatabaseReference usuarioTbl = firebaseReferencia.child("tcsose");
            DatabaseReference usuario = usuarioTbl.child(usuarioPOJO.getChave());
            usuario.removeValue();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
