package com.iteyes.helpinyourday.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.iteyes.helpinyourday.Singleton;
import com.iteyes.helpinyourday.database.FirebaseDatabaseAcess;
import com.iteyes.helpinyourday.old.DbHelper;
import com.iteyes.helpinyourday.pojo.ParceiroPOJO;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;

public class ParceiroDAO implements IdatabaseDAO {

    private SQLiteDatabase read;
    private SQLiteDatabase write;

    private DatabaseReference firebaseReferencia;

    public ParceiroDAO(Context context) {
        firebaseReferencia = FirebaseDatabaseAcess.getFirebaseInstance().getReferencia();
        DbHelper dbHelper = new DbHelper(context);
        write = dbHelper.getWritableDatabase();
        read = dbHelper.getReadableDatabase();
//        getListaParceirosFirebase();
    }

    @Override
    public boolean salvar(ParceiroPOJO parceiroPOJO) {

        ContentValues cv = new ContentValues();
        cv.put(parceiroPOJO.fildCODPARC,parceiroPOJO.getCodParc());
        cv.put(parceiroPOJO.fildNOMEPARC,parceiroPOJO.getNomeParc());
        cv.put(parceiroPOJO.fildATIVO,parceiroPOJO.getAtivo());
        cv.put(parceiroPOJO.fildTIPOPESSOA,parceiroPOJO.getTipoPessoa());
        cv.put(parceiroPOJO.fildMATRIZ,parceiroPOJO.getMatriz());
        cv.put(parceiroPOJO.fildCGC_CPF,parceiroPOJO.getCgc_cpf());
        cv.put(parceiroPOJO.fildCEP,parceiroPOJO.getCep());
        cv.put(parceiroPOJO.fildNUMERO,parceiroPOJO.getNumero());
        cv.put(parceiroPOJO.fildCOMPLEMENTO,parceiroPOJO.getComplemento());
        cv.put(parceiroPOJO.fildTELEFONE,parceiroPOJO.getTelefone());
        cv.put(parceiroPOJO.fildEMAIL,parceiroPOJO.getEmail());
        cv.put(parceiroPOJO.fildLATITUDE,parceiroPOJO.getLatitude());
        cv.put(parceiroPOJO.fildLONGITUDE,parceiroPOJO.getLongitude());

        try {
            write.insert(DbHelper.TABELA_PARCEIRO,null,cv);
//            Log.e("INFO","tarefa inserida com sucesso="+tarefasModel.getDescricaoTarefa());
        }
        catch (Exception e){
            Log.e("INFO","Erro ao Salvar Erro:"+e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public boolean atualizar(ParceiroPOJO parceiroPOJO) {
//        try {
//
//            ContentValues cv = new ContentValues();
//            cv.put("nome",tarefasModel.getDescricaoTarefa());
//            String[] args = {String.valueOf(tarefasModel.getIdTarefa())};
//
//            write.update(DbHelper.TABELA_PARCEIRO,cv,"id = ?",args);
//            Log.e("INFO","tarefa atualiza com sucesso="+tarefasModel.getDescricaoTarefa());
//        }
//        catch (Exception e){
//            Log.e("INFO","Erro ao Atualizar Erro:"+e.getMessage());
//            return false;
//        }
        return false;
    }

    @Override
    public boolean deletar(ParceiroPOJO parceiroPOJO) {
        return false;
    }

    @Override
    public List<ParceiroPOJO> listarParceiros() {

        List<ParceiroPOJO> parceiroPOJOS = new ArrayList<>();

        String SQL = " SELECT CODPARC,NOMEPARC,ATIVO,TIPOPESSOA,MATRIZ,CGC_CPF,CEP,NUMERO,COMPLEMENTO,TELEFONE,EMAIL,LATITUDE,LONGITUDE FROM "+DbHelper.TABELA_PARCEIRO+ " WHERE LATITUDE LIKE '%-%' ORDER BY NOMEPARC ASC;";

        Cursor c = read.rawQuery(SQL,null);

        while(c.moveToNext()){
             ParceiroPOJO parceiroPOJO = new ParceiroPOJO();
             parceiroPOJO.setCodParc(c.getString(c.getColumnIndex(parceiroPOJO.fildCODPARC)));
             parceiroPOJO.setNomeParc(c.getString(c.getColumnIndex(parceiroPOJO.fildNOMEPARC)));
//             parceiroPOJO.setAtivo(c.getString(c.getColumnIndex(parceiroPOJO.ATIVO)));
//             parceiroPOJO.setTipoPessoa(c.getString(c.getColumnIndex(parceiroPOJO.TIPOPESSOA)));
//             parceiroPOJO.setMatriz(c.getInt(c.getColumnIndex(parceiroPOJO.MATRIZ)));
             parceiroPOJO.setCgc_cpf(c.getString(c.getColumnIndex(parceiroPOJO.fildCGC_CPF)));
             parceiroPOJO.setCep(c.getString(c.getColumnIndex(parceiroPOJO.fildCEP)));
             parceiroPOJO.setNumero(c.getString(c.getColumnIndex(parceiroPOJO.fildNUMERO)));
             parceiroPOJO.setComplemento(c.getString(c.getColumnIndex(parceiroPOJO.fildCOMPLEMENTO)));
             parceiroPOJO.setTelefone(c.getString(c.getColumnIndex(parceiroPOJO.fildTELEFONE)));
             parceiroPOJO.setEmail(c.getString(c.getColumnIndex(parceiroPOJO.fildEMAIL)));
             parceiroPOJO.setLatitude(c.getDouble(c.getColumnIndex(parceiroPOJO.fildLATITUDE)));
             parceiroPOJO.setLongitude(c.getDouble(c.getColumnIndex(parceiroPOJO.fildLONGITUDE)));

            parceiroPOJOS.add(parceiroPOJO);
        }

        return parceiroPOJOS;
    }

    public void salvarParceiro(ParceiroPOJO parceiroPOJO){
        DatabaseReference parceirosTbl = firebaseReferencia.child("tgfpar");
        DatabaseReference parceiro = parceirosTbl.child(parceiroPOJO.getCodParc());

        parceiro.child(parceiroPOJO.fildCODPARC).setValue(parceiroPOJO.getCodParc());
        parceiro.child(parceiroPOJO.fildNOMEPARC).setValue(parceiroPOJO.getNomeParc());
        parceiro.child(parceiroPOJO.fildCGC_CPF).setValue(parceiroPOJO.getCgc_cpf());
        parceiro.child(parceiroPOJO.fildNUMERO).setValue(parceiroPOJO.getNumero());
        parceiro.child(parceiroPOJO.fildCOMPLEMENTO).setValue(parceiroPOJO.getComplemento());
        parceiro.child(parceiroPOJO.fildTELEFONE).setValue(parceiroPOJO.getTelefone());
        parceiro.child(parceiroPOJO.fildEMAIL).setValue(parceiroPOJO.getEmail());
        parceiro.child(parceiroPOJO.fildLATITUDE).setValue(parceiroPOJO.getLatitude());
        parceiro.child(parceiroPOJO.fildLONGITUDE).setValue(parceiroPOJO.getLongitude());
        parceiro.child(parceiroPOJO.fildCEP).setValue(parceiroPOJO.getCep());
        parceirosTbl.push();
//        parceirosTbl.setValue(parceiroPOJO.getCodParc());
    }

    public List<ParceiroPOJO> getListaParceirosFirebase(){
        final DatabaseReference tgfpar = firebaseReferencia.child("tgfpar");

        Query parceirosPesquisa = tgfpar.orderByKey().limitToFirst(15);
//        final List<ParceiroPOJO> listaParceiros = new ArrayList<ParceiroPOJO>();

        parceirosPesquisa.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            ParceiroPOJO dadosParceiro = dataSnapshot.getValue(ParceiroPOJO.class);
//            Singleton.getSingleton().getParceirosListFirebase().add(dadosParceiro);

                Log.i("Dados Parceiro",dadosParceiro.toString());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return Singleton.getSingleton().getParceirosListFirebase();
    }
}
