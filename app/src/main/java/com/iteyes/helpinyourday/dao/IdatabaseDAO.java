package com.iteyes.helpinyourday.dao;

import com.iteyes.helpinyourday.pojo.ParceiroPOJO;

import java.util.List;


public interface IdatabaseDAO {

    public boolean salvar(ParceiroPOJO parceiroPOJO);
    public boolean atualizar(ParceiroPOJO parceiroPOJO);
    public boolean deletar(ParceiroPOJO parceiroPOJO);
    public List<ParceiroPOJO> listarParceiros();
}
