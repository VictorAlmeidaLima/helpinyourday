package com.iteyes.helpinyourday.dao;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.util.Base64;

import com.google.firebase.database.DatabaseReference;
import com.iteyes.helpinyourday.database.FirebaseDatabaseAcess;
import com.iteyes.helpinyourday.helper.Base64Custom;
import com.iteyes.helpinyourday.model.UsuarioModel;
import com.iteyes.helpinyourday.old.DbHelper;
import com.iteyes.helpinyourday.pojo.OrdemServicoPOJO;
import com.iteyes.helpinyourday.pojo.UsuarioPOJO;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.RequiresApi;

public class OrdemServicoDAO {
    private SQLiteDatabase read;
    private SQLiteDatabase write;

    private DatabaseReference firebaseReferencia;

    public OrdemServicoDAO(Context context) {
        firebaseReferencia = FirebaseDatabaseAcess.getFirebaseInstance().getReferencia();
        DbHelper dbHelper = new DbHelper(context);
        write = dbHelper.getWritableDatabase();
        read = dbHelper.getReadableDatabase();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void salvarOrdemServicoUsuarioMesAno(OrdemServicoPOJO ordemServico) {
        UsuarioModel usuarioModel = new UsuarioModel();
        String emailBase64 = Base64Custom.codificarStrintToBase64(usuarioModel.getUsuarioPOJO().getEmail());
        String mesAno = ordemServico.getMesAno();

        if("".equals(ordemServico.getChave()) || ordemServico.getChave() == null ) {

            firebaseReferencia.child("tcsose")
                    .child(emailBase64)
                    .child(mesAno)
                    .push()
                    .setValue(ordemServico);
        }else{
            firebaseReferencia.child("tcsose")
                    .child(emailBase64)
                    .child(mesAno)
                    .child(ordemServico.getChave())
                    .setValue(ordemServico);
        }
    }


    public void salvarOrdemServico(OrdemServicoPOJO ordemServicoPOJO) {
        UsuarioModel usuarioModel = new UsuarioModel();
        UsuarioPOJO usuarioLogado = usuarioModel.getUsuarioPOJO();

        DatabaseReference ordemServicoTbl = firebaseReferencia.child("tcsose");
        DatabaseReference ordemServico = null;

        if ("".equals(ordemServicoPOJO.getChave()) || ordemServicoPOJO.getChave() == null) {
            String ordemServicoKEY = ordemServicoTbl.push().getKey();
            ordemServico = ordemServicoTbl.child(ordemServicoKEY);

        } else {
            ordemServico = ordemServicoTbl.child(ordemServicoPOJO.getChave());
        }

        ordemServico.setValue(ordemServicoPOJO);
        ordemServicoTbl.push();

    }

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.O)
    public boolean deletarOrdemServico(OrdemServicoPOJO ordemServicoPOJO) {
        UsuarioModel usuarioModel = new UsuarioModel();
        String emailBase64 = Base64Custom.codificarStrintToBase64(usuarioModel.getUsuarioPOJO().getEmail());
        String mesAno = ordemServicoPOJO.getMesAno();

        try {
            DatabaseReference ordemServicoTbl = firebaseReferencia.child("tcsose").child(emailBase64).child(mesAno);
            DatabaseReference ordemServico = ordemServicoTbl.child(ordemServicoPOJO.getChave());
            ordemServico.removeValue();
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    public List<OrdemServicoPOJO> listaOrdemServico() {
        List<OrdemServicoPOJO> ordemServicoPOJOS = new ArrayList<>();

        String SQL = " SELECT NUOS,NUMOS,DESCRICAO,DTOS,DHINICIO,DHFINAL FROM " + DbHelper.TABELA_OS + " ORDER BY DTOS DESC;";

        Cursor c = read.rawQuery(SQL, null);

        while (c.moveToNext()) {
            OrdemServicoPOJO ordemServicoPOJO = new OrdemServicoPOJO();

//            ordemServicoPOJO.setNuOs(c.getString(c.getColumnIndex(ordemServicoPOJO.fildNUOS)));
//            ordemServicoPOJO.setNumeroOs(c.getString(c.getColumnIndex(ordemServicoPOJO.fildNUMOS)));
//            ordemServicoPOJO.setDataOs(c.getString(c.getColumnIndex(ordemServicoPOJO.fildDTOS)));
//            ordemServicoPOJO.setDescricao(c.getString(c.getColumnIndex(ordemServicoPOJO.fildDESCRICAO)));
//            ordemServicoPOJO.setDhInicio(c.getString(c.getColumnIndex(ordemServicoPOJO.fildDHINICIO)));
//            ordemServicoPOJO.setDhFinal(c.getString(c.getColumnIndex(ordemServicoPOJO.fildDHFINAL)));

            ordemServicoPOJOS.add(ordemServicoPOJO);
        }

        OrdemServicoPOJO ordemServicoPOJO = new OrdemServicoPOJO();
        ordemServicoPOJO.setNuOs("1");
        ordemServicoPOJO.setDescricao("Ordem Servico Nr1");
        ordemServicoPOJO.setDhInicio("08:00:00");
        ordemServicoPOJO.setDhFinal("18:00:00");
        ordemServicoPOJO.setCodParc("MG MIX");

        ordemServicoPOJOS.add(ordemServicoPOJO);

        return ordemServicoPOJOS;
    }
}
