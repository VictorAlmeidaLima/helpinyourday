package com.iteyes.helpinyourday;

import com.iteyes.helpinyourday.pojo.OrdemServicoPOJO;
import com.iteyes.helpinyourday.pojo.ParceiroPOJO;

import java.util.List;

public class Singleton {
    private static Singleton instance;
    private List<ParceiroPOJO> parceirosList;
    private List<ParceiroPOJO> parceirosListFirebase;
    private ParceiroPOJO activeParceiro;

    private List<OrdemServicoPOJO> ordenServicoList;
    private OrdemServicoPOJO activeOrdemServico;


    private Singleton(){
    }

    public static synchronized Singleton getSingleton(){
        if(instance == null){
            instance = new Singleton();
        }
        return instance;
    }

    public List<ParceiroPOJO> getParceirosListFirebase() {
        return parceirosListFirebase;
    }

    public void setParceirosListFirebase(List<ParceiroPOJO> parceirosListFirebase) {
        this.parceirosListFirebase = parceirosListFirebase;
    }

    public List<ParceiroPOJO> getParceirosList() {
        return parceirosList;
    }

    public void setParceirosList(List<ParceiroPOJO> parceirosList) {
        this.parceirosList = parceirosList;
    }

    public ParceiroPOJO getActiveParceiro() {
        return activeParceiro;
    }

    public void setActiveParceiro(ParceiroPOJO activeParceiro) {
        this.activeParceiro = activeParceiro;
    }

    public void setActiveParceiro(int index) {
        this.activeParceiro = getParceirosList().get(index);
    }

    public List<OrdemServicoPOJO> getOrdenServicoList() {
        return ordenServicoList;
    }

    public void setOrdenServicoList(List<OrdemServicoPOJO> ordenServicoList) {
        this.ordenServicoList = ordenServicoList;
    }

    public OrdemServicoPOJO getActiveOrdemServico() {
        return activeOrdemServico;
    }

    public void setActiveOrdemServico(OrdemServicoPOJO activeOrdemServico) {
        this.activeOrdemServico = activeOrdemServico;
    }

    public void setActiveOrdemServico(int index) {
        this.activeOrdemServico = getOrdenServicoList().get(index);
    }
}