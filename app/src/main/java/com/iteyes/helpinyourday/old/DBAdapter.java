package com.iteyes.helpinyourday.old;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
        import android.database.Cursor;
        import android.database.SQLException;
        import android.database.sqlite.SQLiteDatabase;
        import android.util.Log;

import com.iteyes.helpinyourday.pojo.ParceiroPOJO;

public class DBAdapter
{
    protected static final String TAG = "DBAdapter";

    private final Context mContext;
    private SQLiteDatabase mDb;
    private DBConnection mDbHelper;

    public DBAdapter(Context context)
    {
        this.mContext = context;
        mDbHelper = new DBConnection(mContext);
    }

    public DBAdapter createDatabase() throws SQLException
    {
        try
        {
            mDbHelper.createDataBase();
        }
        catch (IOException mIOException)
        {
            Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase");
            throw new Error("UnableToCreateDatabase");
        }
        return this;
    }

    public DBAdapter open() throws SQLException
    {
        try
        {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getReadableDatabase();
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "open >>"+ mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    public void close()
    {
        mDbHelper.close();
    }


    public List<ParceiroPOJO> listarParceiros() {

        List<ParceiroPOJO> parceiroPOJOS = new ArrayList<>();

        String SQL = " SELECT CODPARC,NOMEPARC,ATIVO,TIPOPESSOA,MATRIZ,CGC_CPF,CEP,NUMERO,COMPLEMENTO,TELEFONE,EMAIL,LATITUDE,LONGITUDE FROM "+ DbHelper.TABELA_PARCEIRO+ " WHERE CODPARC IN (26763,26747,26717,26701,26688,26686); ";

        Cursor c = this.mDb.rawQuery(SQL,null);

        while(c.moveToNext()){
            ParceiroPOJO parceiroPOJO = new ParceiroPOJO();

//            parceiroPOJO.setCodParc(c.getInt(c.getColumnIndex(parceiroPOJO.CODPARC)));
//            parceiroPOJO.setNomeParc(c.getString(c.getColumnIndex(parceiroPOJO.NOMEPARC)));
//            parceiroPOJO.setAtivo(c.getString(c.getColumnIndex(parceiroPOJO.ATIVO)));
//            parceiroPOJO.setTipoPessoa(c.getString(c.getColumnIndex(parceiroPOJO.TIPOPESSOA)));
//            parceiroPOJO.setMatriz(c.getInt(c.getColumnIndex(parceiroPOJO.MATRIZ)));
//            parceiroPOJO.setCgc_cpf(c.getString(c.getColumnIndex(parceiroPOJO.CGC_CPF)));
//            parceiroPOJO.setNumero(c.getString(c.getColumnIndex(parceiroPOJO.NUMERO)));
//            parceiroPOJO.setComplemento(c.getString(c.getColumnIndex(parceiroPOJO.COMPLEMENTO)));
//            parceiroPOJO.setTelefone(c.getString(c.getColumnIndex(parceiroPOJO.TELEFONE)));
//            parceiroPOJO.setEmail(c.getString(c.getColumnIndex(parceiroPOJO.EMAIL)));
//            parceiroPOJO.setLatitude(BigDecimal.valueOf((c.getColumnIndex(parceiroPOJO.LATITUDE))));
//            parceiroPOJO.setLongitude(BigDecimal.valueOf(c.getColumnIndex(parceiroPOJO.LONGITUDE)));

            parceiroPOJOS.add(parceiroPOJO);
        }

        return parceiroPOJOS;
    }

    public Cursor getTestData()
    {
        try
        {
            String sql ="SELECT * FROM TGFPAR ";

            Cursor mCur = mDb.rawQuery(sql, null);
            if (mCur!=null)
            {
                mCur.moveToNext();
            }
            return mCur;
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "getTestData >>"+ mSQLException.toString());
            throw mSQLException;
        }
    }
}
