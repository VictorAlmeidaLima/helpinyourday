package com.iteyes.helpinyourday.old;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.iteyes.helpinyourday.R;
import com.iteyes.helpinyourday.database.InsertParceiro;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by jamiltondamasceno
 */

public class DbHelper extends SQLiteOpenHelper {

    public static int VERSION = 1;
    public static String NOME_DB = "PROD.db";
    public static String TABELA_PARCEIRO = "TGFPARBH";
    public static String TABELA_OS = "TCSOSEBH";
    private static String DB_PATH = "";

    public DbHelper(Context context) {
        super(context, NOME_DB, null, VERSION);
        DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        criarTabelaParceiros(db);
        criarTabelaOrdemServico(db);

    }

    private void criarTabelaParceiros(SQLiteDatabase db) {
        String sql = "CREATE TABLE "+TABELA_PARCEIRO+" ( \n" +
                "    CODPARC     INT          PRIMARY KEY ON CONFLICT ROLLBACK, \n" +
                "    NOMEPARC    STRING (200), \n" +
                "    ATIVO       STRING (3), \n" +
                "    TIPOPESSOA  STRING (15), \n" +
                "    MATRIZ      INT, \n" +
                "    CGC_CPF     STRING (30), \n" +
                "    CEP         STRIING (20), \n" +
                "    NUMERO      STRING (15), \n" +
                "    COMPLEMENTO STRING (50), \n" +
                "    TELEFONE    STRING (20), \n" +
                "    EMAIL       STRING (50), \n" +
                "    LATITUDE    STRING (30), \n" +
                "    LONGITUDE   STRING (30)  \n" +
                "); ";

        try {
            db.execSQL( sql );
            Log.i("INFO DB", "Sucesso ao criar a tabela" );
            inserirRegistrosPrimarios(db);
        }catch (Exception e){
            Log.i("INFO DB", "Erro ao criar a tabela" + e.getMessage() );
        }finally {

        }
    }
    private void criarTabelaOrdemServico(SQLiteDatabase db) {
        String sql = "CREATE TABLE TCSOSEBH (\n" +
                "    NUOS      INTEGER      PRIMARY KEY ASC AUTOINCREMENT,\n" +
                "    NUMOS     STRING (50),\n" +
                "    DESCRICAO STRING (400),\n" +
                "    DTOS      DATE,\n" +
                "    DHINICIO  DATETIME,\n" +
                "    DHFINAL   DATETIME\n" +
                ");";

        try {
            db.execSQL( sql );
            Log.i("INFO DB", "Sucesso ao criar a tabela" );
//            inserirRegistrosPrimarios(db);
        }catch (Exception e){
            Log.i("INFO DB", "Erro ao criar a tabela" + e.getMessage() );
        }finally {

        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String sql = "DROP TABLE IF EXISTS " + TABELA_PARCEIRO + " ;" ;

        try {
            db.execSQL( sql );
            onCreate(db);
            Log.i("INFO DB", "Sucesso ao atualizar App" );
        }catch (Exception e){
            Log.i("INFO DB", "Erro ao atualizar App" + e.getMessage() );
        }

    }

    public static String convertStreamToString(InputStream is) throws IOException {
        // http://www.java2s.com/Code/Java/File-Input-Output/ConvertInputStreamtoString.htm
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        Boolean firstLine = true;
        while ((line = reader.readLine()) != null) {
            if(firstLine){
                sb.append(line);
                firstLine = false;
            } else {
                sb.append("\n").append(line);
            }
        }
        reader.close();
        return sb.toString();
    }

    public List<String> getInsertSQL() throws IOException {
//        File file = checkFileInsert();
//        String stringFromFile = getStringFromFile(file.getPath());
//        return stringFromFile;

        return InsertParceiro.getInsert();
    }

    //verifica se a base existe na pasta: /data/data/your package/databases/Da Name
    private File checkFileInsert()
    {
        File file = new File(DB_PATH + "queinsert.sql");
        //Log.v("dbFile", dbFile + "   "+ dbFile.exists());
        if(file.exists()){
            return file;
        }

        return null;
    }

    public static String getStringFromFile (String filePath) throws IOException {
        File fl = new File(filePath);
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        //Make sure you close all streams.
        fin.close();
        return ret;
    }


    public void inserirRegistrosPrimarios(SQLiteDatabase db) throws Exception {

        List<String> listaString;
        listaString = getInsertSQL();

        for (String insertSQL : listaString) {
            try {
                 db.execSQL( insertSQL );
                Log.i("INFO DB", "Registros Primários inseridos" );
            }catch (Exception e){
                Log.i("INFO DB", "Erro ao inserir Registros" + e.getMessage() );
            }finally {
                continue;
            }
        }

    }

}
