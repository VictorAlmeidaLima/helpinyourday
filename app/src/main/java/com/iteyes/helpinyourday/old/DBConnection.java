package com.iteyes.helpinyourday.old;




import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
        import java.io.IOException;
        import java.io.InputStream;
        import java.io.OutputStream;

        import android.content.Context;
        import android.database.SQLException;
        import android.database.sqlite.SQLiteDatabase;
        import android.database.sqlite.SQLiteOpenHelper;
        import android.util.Log;

import static com.iteyes.helpinyourday.old.DbHelper.convertStreamToString;


public class DBConnection extends SQLiteOpenHelper {

    private static String TAG = "DBHelper"; // LogCat
    //local da sua base no smartphone
    private static String DB_PATH = "";
    private static String DB_NAME ="PROD.db";// o nome do seu banco
    private SQLiteDatabase mDataBase;
    private final Context mContext;

    public DBConnection(Context context)
    {
        super(context, DB_NAME, null, 2);// 1 = versão da base
        if(android.os.Build.VERSION.SDK_INT >= 17){
            DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
        }
        else
        {
            DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        }
        this.mContext = context;
    }

    public void createDataBase() throws IOException
    {
        //se a base não existe, copia da pasta assets.

        boolean mDataBaseExist = checkDataBase();
        if(!mDataBaseExist)
        {
            this.getReadableDatabase();
            this.close();
            try
            {
                //copia a base
                copyDataBase();
                Log.e(TAG, "createDatabase base criada");
            }
            catch (IOException mIOException)
            {
                throw new Error("Erro copiando a base"+mIOException.getMessage());
            }
        }else
        {
            criarTabelaParceiros(this.mDataBase);
        }
    }

    //verifica se a base existe na pasta: /data/data/your package/databases/Da Name
    private boolean checkDataBase()
    {
        File dbFile = new File(DB_PATH + DB_NAME);
        //Log.v("dbFile", dbFile + "   "+ dbFile.exists());
        return dbFile.exists();

    }

    //verifica se a base existe na pasta: /data/data/your package/databases/Da Name
    private File checkFileInsert()
    {
        File file = new File(DB_PATH + "queinsert.sql");
        //Log.v("dbFile", dbFile + "   "+ dbFile.exists());
        if(file.exists()){
            return file;
        }

        return null;
    }

    //Copia a base da pasta assets
    private void copyDataBase() throws IOException
    {
        InputStream mInput = mContext.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream mOutput = new FileOutputStream(outFileName);
        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer))>0)
        {
            mOutput.write(mBuffer, 0, mLength);
        }
        mOutput.flush();
        mOutput.close();
        mInput.close();
    }

    //Abra a base de dados pra fazer consulta
    public boolean openDataBase() throws SQLException
    {
        String mPath = DB_PATH + DB_NAME;
        //Log.v("mPath", mPath);
        mDataBase = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.CREATE_IF_NECESSARY);
        //mDataBase = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
        return mDataBase != null;
    }

    @Override
    public synchronized void close()
    {
        if(mDataBase != null)
            mDataBase.close();
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        criarTabelaParceiros(db);
    }

    private void criarTabelaParceiros(SQLiteDatabase db) {
        String sql = "CREATE TABLE TGFPAR ( \n" +
                "    CODPARC     INT          PRIMARY KEY ON CONFLICT ROLLBACK, \n" +
                "    NOMEPARC    STRING (200), \n" +
                "    ATIVO       STRING (3), \n" +
                "    TIPOPESSOA  STRING (15), \n" +
                "    MATRIZ      INT, \n" +
                "    CGC_CPF     STRING (30), \n" +
                "    CEP         STRIING (20), \n" +
                "    NUMERO      STRING (15), \n" +
                "    COMPLEMENTO STRING (50), \n" +
                "    TELEFONE    STRING (20), \n" +
                "    EMAIL       STRING (50), \n" +
                "    LATITUDE    STRING (30), \n" +
                "    LONGITUDE   STRING (30)  \n" +
                "); ";

        try {
            db.execSQL( sql );
            Log.i("INFO DB", "Sucesso ao criar a tabela" );
        }catch (Exception e){
            Log.i("INFO DB", "Erro ao criar a tabela" + e.getMessage() );
        }finally {
            inserirRegistrosPrimarios(db);
        }
    }

    public String getInsertSQL() throws IOException {
        File file = checkFileInsert();
        String stringFromFile = getStringFromFile(file.getPath());
        return stringFromFile;
    }

    public static String getStringFromFile (String filePath) throws IOException {
        File fl = new File(filePath);
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        //Make sure you close all streams.
        fin.close();
        return ret;
    }

    public void inserirRegistrosPrimarios(SQLiteDatabase db){

        String insertSQL;

        try {
            insertSQL = getInsertSQL();
            db.execSQL( insertSQL );
            Log.i("INFO DB", "Registros Primários inseridos" );
        }catch (Exception e){
            Log.i("INFO DB", "Erro ao inserir Registros" + e.getMessage() );
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(newVersion>oldVersion) {
            try {
                copyDataBase();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
