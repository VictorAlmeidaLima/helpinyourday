package com.iteyes.helpinyourday.helper;

import android.icu.util.Calendar;
import android.os.Build;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.GregorianCalendar;

import androidx.annotation.RequiresApi;

public abstract class TimeUtils {

    public static String somarHoras(String dhInicio,String dhFinal){
        String duracao = "";

        GregorianCalendar gc = new GregorianCalendar();

        int hora = Integer.parseInt(dhInicio.substring(0,2));
        int min = Integer.parseInt(dhInicio.substring(3,5));
        int seg = Integer.parseInt(dhInicio.substring(6,8));

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

        Time time = new Time(hora, min, seg);
        gc.setTimeInMillis(time.getTime());

        hora = Integer.parseInt(dhFinal.substring(0,2));
        min = Integer.parseInt(dhFinal.substring(3,5));
        seg = Integer.parseInt(dhInicio.substring(6,8));

        gc.add(Calendar.HOUR,hora);
        gc.add(Calendar.MINUTE,min);
        gc.add(Calendar.SECOND,seg);

        duracao = sdf.format(gc.getTime());

        return duracao;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static String getDuracaoHoras(String dhInicio, String dhFinal){
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        GregorianCalendar gc = new GregorianCalendar();

        // também tem outros construtores para utilizar números
        LocalTime lt1 = LocalTime.parse(dhInicio);
        LocalTime lt2 = LocalTime.parse(dhFinal);
        // diferenca
        int emHoras = Integer.valueOf((int) lt1.until(lt2, ChronoUnit.HOURS));
        int emMinutos = (int) lt1.until(lt2, ChronoUnit.MINUTES);
        int emSegundos = (int) lt1.until(lt2, ChronoUnit.SECONDS);

        Time time = new Time(emHoras, emMinutos, emSegundos);
        gc.setTimeInMillis(time.getTime());

        return sdf.format(gc.getTime());
    }
}
