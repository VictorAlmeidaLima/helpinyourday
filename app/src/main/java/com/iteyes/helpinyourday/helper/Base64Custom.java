package com.iteyes.helpinyourday.helper;

import android.os.Build;

import java.util.Base64;

import androidx.annotation.RequiresApi;

public abstract class Base64Custom {

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static String decodificarBase64toString(String str) {
        String result = "";
        Base64.Decoder decoder = Base64.getDecoder();
        result = new String(decoder.decode(str));
        return result;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static String codificarStrintToBase64(String str) {
        String result = "";
        Base64.Encoder encoder = Base64.getEncoder();
        result = encoder.encodeToString(str.getBytes());
        return result;
    }
}
