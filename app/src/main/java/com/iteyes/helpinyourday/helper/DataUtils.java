package com.iteyes.helpinyourday.helper;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class DataUtils {

    public static String getMesAnoString(String data) {
        String dataQuebrada[] = data.split("/");
        String mes = dataQuebrada[1];
        String ano = dataQuebrada[2];

        return mes + ano;
    }

    public static String getDataAtual() {
        String data = "";

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date dt = new Date();
        data = format.format(dt);

        return data;
    }

    public static String formataData(Date dt) {
        String data = "";

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        data = format.format(dt);

        return data;
    }

    public static String getMesAnoAtual() {
        String mesAno = "";

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date dt = new Date();
        mesAno = getMesAnoString(format.format(dt));

        return mesAno;
    }
}
